#!/bin/sh

perl -pi -e 's/us.archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list
apt-get -y update > /dev/null

# Install & Setup Apache
apt-get -q -y install libapache2-mod-passenger > /dev/null
a2enmod passenger > /dev/null
a2enmod rewrite > /dev/null

# Add PPA repository for Redmine latest version
DEBIAN_FRONTEND=noninteractive apt-get -q -y install python-software-properties > /dev/null
add-apt-repository ppa:ondrej/redmine -y > /dev/null
apt-get -q -y update > /dev/null

# Install Redmine
DEBIAN_FRONTEND=noninteractive apt-get -q -y install ruby-bundler ruby-fastercsv > /dev/null
DEBIAN_FRONTEND=noninteractive apt-get -q -y install redmine > /dev/null
mv /var/www /var/www.orig
ln -s /usr/share/redmine/public /var/www
chgrp www-data /usr/share/redmine
chmod g+w /usr/share/redmine

# Setup Passenger
patch /etc/apache2/mods-available/passenger.conf < /vagrant/passenger.conf.patch
patch /etc/apache2/sites-available/default < /vagrant/default.patch

apt-get -q -y install git
mkdir /usr/share/redmine/plugins
cd /usr/share/redmine/plugins

# scrum2b - not work
#git clone https://github.com/scrum2b/scrum2b.git
#cd /usr/share/redmine
#bundle install
#bundle exec rake redmine:plugins:migrate RAILS_ENV=production
#echo "Visit http://127.0.0.1:8080/settings/plugin/scrum2b as admin/admin"

# Agile Dwarf
git clone https://github.com/iRessources/AgileDwarf.git
cd /usr/share/redmine
RAILS_ENV=production rake redmine:plugins:migrate

service apache2 restart
